#!/bin/bash

echo "Concatenating files..."
cat lts/index.md lts/membership.md lts/acquisition.md lts/cataloguing.md lts/circulation.md >lts/README.md && echo "LTS successfulyl concatenated"
cat fsp/index.md fsp/membership.md fsp/acquisition.md fsp/cataloguing.md fsp/circulation.md >fsp/README.md && echo "FSP successfulyl concatenated"

bfdocs --no-header ./manifest.json docs
