### Membership component

```
    States = {idle, registering, authenticating, updating}
    s0 = idle
    L = {register, authenticate, update}
    δ = {
        (idle, register, idle),
        (idle, authenticate, idle),
        (idle, update, idle)
    }

    Register = (
          {idle, registering, authenticating, updating}
        , idle
        , {register, authenticate, update},
        , {
              (idle, register, idle)
            , (idle, authenticate, idle)
            , (idle, update, idle)
          }
    )
```
