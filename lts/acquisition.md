### Acquisition component


    States = {idle, receiving_bid, assigning_supplier, receiving_delivery, ordering, invoicing, sending_receipt}
    s0 = idle
    L = {receive_bid, assign_supplier, receive_delivery, order, invoice, send_receipt}
    δ = {
          (idle, receive_bid, idle)
        , (idle, assign_supplier, idle)
        , (idle, receive_delivery, send_receipt)
        , (receive_delivery, send_receipt, idle)
        , (idle, order, invoice)
        , (order, invoice, idle)
    }

    Acquisition = (
          {idle, receiving_bid, assigning_supplier, receiving_delivery, ordering, invoicing, sending_receipt}
        , idle
        , {receive_bid, assign_supplier, receive_delivery, order, invoice, send_receipt}
        , {
              (idle, receive_bid, idle)
            , (idle, assign_supplier, idle)
            , (idle, receive_delivery, send_receipt)
            , (receive_delivery, send_receipt, idle)
            , (idle, order, invoice)
            , (order, invoice, idle)
          }
    )
