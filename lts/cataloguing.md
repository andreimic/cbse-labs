### Cataloguing component

```
    States = {idle, adding, managing, searching}
    s0 = idle
    L = {add, manage, search}
    δ = {
          (idle, add, idle)
        , (idle, manage, idle)
        , (idle, search, idle)
    }

    Cataloguing = (
          {idle, adding, managing, searching}
        , idle
        , {add, manage, search}
        , {
              (idle, add, idle)
            , (idle, manage, idle)
            , (idle, search, idle)
          }
    )
```
