### Circulation component

```
    States = {idle, lending, returning, renewing, reserving}
    s0 = idle
    L = {lend, return, renew, reserve}
    δ = {
          (idle, lend, idle)
        , (idle, return, idle)
        , (idle, renew, idle)
        , (idle, reserve, idle)
    }

    Circulation = (
          {idle, lending, returning, renewing, reserving}
        , idle
        , {lend, return, renew, reserve}
        , {
              (idle, lend, idle)
            , (idle, return, idle)
            , (idle, renew, idle)
            , (idle, reserve, idle)
          }
    )
```
